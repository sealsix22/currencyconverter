package controller;

import configuration.TestContextConfiguration;
import cz.komercpoj.currencyconverter.CurrencyConverterApplication;
import cz.komercpoj.currencyconverter.controller.CurrenciesRestV1Controller;
import cz.komercpoj.currencyconverter.to.ConvertInputTO;
import cz.komercpoj.currencyconverter.to.ConvertResultTO;
import cz.komercpoj.currencyconverter.to.CurrencyFindResultTO;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = {"classpath:application.properties"})
@TestPropertySource(locations = {"classpath:application-test.properties"})
@ContextConfiguration(classes = TestContextConfiguration.class)
@SpringBootTest(classes = CurrencyConverterApplication.class)
@FixMethodOrder(MethodSorters.DEFAULT)
public class CurrenciesRestV1ControllerIntegrationTest {

    @Autowired
    private CurrenciesRestV1Controller currenciesRestV1Controller;

    @Test
    public void testConvert1() {
        final ConvertInputTO inputTO = new ConvertInputTO();
        inputTO.setFrom("CZK");
        inputTO.setTo("EUR");
        inputTO.setValue(BigDecimal.valueOf(135.69));

        final ResponseEntity<ConvertResultTO> r = currenciesRestV1Controller.convert(inputTO);
        assertEquals(r.getStatusCode(), HttpStatus.OK);
        assertEquals(r.getBody().getResult(), BigDecimal.valueOf(4.91));
    }

    @Test
    public void testConvert2() {
        final ResponseEntity<List<CurrencyFindResultTO>> r = currenciesRestV1Controller.findAll(Optional.of(LocalDate.now()));

        assertEquals(r.getStatusCode(), HttpStatus.OK);
        assertTrue(!r.getBody().isEmpty());
        assertEquals(r.getBody().size(), 1);
        assertEquals(r.getBody().get(0).getExchangeRate(), BigDecimal.valueOf(27.66));
    }
}
