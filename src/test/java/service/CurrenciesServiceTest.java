package service;

import cz.komercpoj.currencyconverter.dao.repository.ICurrencyPairCourseRepository;
import cz.komercpoj.currencyconverter.domain.entity.CurrencyPairCourse;
import cz.komercpoj.currencyconverter.domain.entity.codelist.CurrencyPair;
import cz.komercpoj.currencyconverter.service.CurrenciesService;
import cz.komercpoj.currencyconverter.to.ConvertInputTO;
import cz.komercpoj.currencyconverter.to.ConvertResultTO;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class CurrenciesServiceTest {

    @Mock
    private ICurrencyPairCourseRepository currencyPairCourseRepository;

    @InjectMocks
    private CurrenciesService currenciesService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFind() {
        LocalDate now = LocalDate.now();

        final CurrencyPairCourse cpc = new CurrencyPairCourse();
        cpc.setExchangeRate(BigDecimal.valueOf(135.69));
        cpc.setPublishedDate(now.minusDays(1));
        CurrencyPair cp = new CurrencyPair();
        cp.setFromCurrencyCode("CZK");
        cp.setToCurrencyCode("EUR");
        cpc.setCurrencyPair(cp);

        when(currencyPairCourseRepository.findByCurrenciesAndMaxDate("EUR","CZK", now)).thenReturn(Optional.of(cpc));

        final ConvertInputTO inputTO = new ConvertInputTO();
        inputTO.setFrom("EUR");
        inputTO.setTo("CZK");
        inputTO.setValue(BigDecimal.valueOf(135.69));

        Optional<ConvertResultTO> res = currenciesService.convertCurrency(inputTO);

        verify(currencyPairCourseRepository, times(1)).findByCurrenciesAndMaxDate("EUR","CZK", now);
        verifyNoMoreInteractions(currencyPairCourseRepository);

        assertNotNull(res);
        assertTrue(res.isPresent());
        assertEquals(res.get().getResult(), BigDecimal.valueOf(18411.7761));

    }

}
