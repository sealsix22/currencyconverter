package dao.repository;


import configuration.TestContextConfiguration;
import cz.komercpoj.currencyconverter.CurrencyConverterApplication;
import cz.komercpoj.currencyconverter.service.ICurrenciesService;
import cz.komercpoj.currencyconverter.to.ConvertInputTO;
import cz.komercpoj.currencyconverter.to.ConvertResultTO;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = {"classpath:application.properties"})
@TestPropertySource(locations = {"classpath:application-test.properties"})
@ContextConfiguration(classes = TestContextConfiguration.class)
@SpringBootTest(classes = CurrencyConverterApplication.class)
@FixMethodOrder(MethodSorters.DEFAULT)
public class CurrenciesServiceIntegrationTest {

    @Autowired
    private ICurrenciesService currenciesService;

    @Test
    public void testConvert1() {
        final ConvertInputTO inputTO = new ConvertInputTO();
        inputTO.setFrom("CZK");
        inputTO.setTo("EUR");
        inputTO.setValue(BigDecimal.valueOf(135.69));

        final Optional<ConvertResultTO> r = currenciesService.convertCurrency(inputTO);
        assertTrue(r.isPresent());
        assertEquals(r.get().getResult(), BigDecimal.valueOf(4.91));
    }

    @Test
    public void testConvert2() {
        final ConvertInputTO inputTO = new ConvertInputTO();
        inputTO.setFrom("CZK");
        inputTO.setTo("EUR");
        inputTO.setDate(LocalDate.now());
        inputTO.setValue(BigDecimal.valueOf(135.69));

        final Optional<ConvertResultTO> r = currenciesService.convertCurrency(inputTO);
        assertTrue(r.isPresent());
        assertEquals(r.get().getResult(), BigDecimal.valueOf(4.91));
    }

    @Test
    public void testConvert3() {
        final ConvertInputTO inputTO = new ConvertInputTO();
        inputTO.setFrom("EUR");
        inputTO.setTo("CZK");
        inputTO.setDate(LocalDate.parse("2020-02-11"));
        inputTO.setValue(BigDecimal.valueOf(4.91));

        final Optional<ConvertResultTO> r = currenciesService.convertCurrency(inputTO);
        assertTrue(r.isPresent());
        assertEquals(r.get().getResult(), BigDecimal.valueOf(123.34411));
    }

    @Test
    public void testConvert4() {
        final ConvertInputTO inputTO = new ConvertInputTO();
        inputTO.setFrom("EUR");
        inputTO.setTo("CZK");
        inputTO.setDate(LocalDate.parse("2020-02-12"));
        inputTO.setValue(BigDecimal.valueOf(4.91));

        final Optional<ConvertResultTO> r = currenciesService.convertCurrency(inputTO);
        assertTrue(r.isPresent());
        assertEquals(r.get().getResult(), BigDecimal.valueOf(123.9284));
    }


}
