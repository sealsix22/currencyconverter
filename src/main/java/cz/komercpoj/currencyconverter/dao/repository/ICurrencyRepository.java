package cz.komercpoj.currencyconverter.dao.repository;

import cz.komercpoj.currencyconverter.domain.entity.CurrencyPairCourse;
import cz.komercpoj.currencyconverter.domain.entity.codelist.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Repository for handling @{@link Currency entity}
 */
@Repository
public interface ICurrencyRepository extends JpaRepository<Currency, String> {

    @Query("select c.code from CURRENCY c")
    List<String> findAllCurrencyCodes();
}
