package cz.komercpoj.currencyconverter.dao.repository;

import cz.komercpoj.currencyconverter.domain.entity.CurrencyPairCourse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Repository for handling @{@link CurrencyPairCourse entity}
 */
@Repository
public interface ICurrencyPairCourseRepository extends JpaRepository<CurrencyPairCourse, Long> {

    @Query(value = "select * from CURRENCY_PAIR_COURSE cpc " +
                    "join CURRENCY_PAIR cp on cp.id = cpc.CURRENCY_PAIR_ID " +
                    "where ((cp.FROM_CURRENCY_CODE = ?1 and cp.TO_CURRENCY_CODE = ?2) or " +
                           "(cp.TO_CURRENCY_CODE = ?1 and cp.FROM_CURRENCY_CODE = ?2)) " +
                           "and cpc.PUBLISHED_DATE = (select max(c.PUBLISHED_DATE) " +
                                                     "from CURRENCY_PAIR_COURSE c " +
                                                     "where c.PUBLISHED_DATE <= ?3) ", nativeQuery = true)
    Optional<CurrencyPairCourse> findByCurrenciesAndMaxDate(String from, String to, LocalDate publishedDate);

    @Query(value = "select * from CURRENCY_PAIR_COURSE cpc " +
                    "join CURRENCY_PAIR cp on cp.id = cpc.CURRENCY_PAIR_ID " +
                    "where cpc.PUBLISHED_DATE = (select max(c.PUBLISHED_DATE) " +
                                                "from CURRENCY_PAIR_COURSE c " +
                                                "where c.PUBLISHED_DATE <= ?1) ", nativeQuery = true)
    List<CurrencyPairCourse> findAllByCurrenciesAndMaxDate(LocalDate publishedDate);

}
