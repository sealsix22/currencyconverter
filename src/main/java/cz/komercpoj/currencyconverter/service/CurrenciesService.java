package cz.komercpoj.currencyconverter.service;

import cz.komercpoj.currencyconverter.dao.repository.ICurrencyPairCourseRepository;
import cz.komercpoj.currencyconverter.dao.repository.ICurrencyRepository;
import cz.komercpoj.currencyconverter.domain.entity.CurrencyPairCourse;
import cz.komercpoj.currencyconverter.to.CurrencyFindResultTO;
import cz.komercpoj.currencyconverter.to.ConvertInputTO;
import cz.komercpoj.currencyconverter.to.ConvertResultTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CurrenciesService implements ICurrenciesService {

    private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_DOWN;

    @Autowired
    private ICurrencyPairCourseRepository currencyPairCourseRepository;

    @Autowired
    private ICurrencyRepository currencyRepository;

    @Override
    public Optional<ConvertResultTO> convertCurrency(final ConvertInputTO inputTO) {
        final LocalDate conversionDate = (inputTO.getDate() != null) ? inputTO.getDate() : LocalDate.now();

        return currencyPairCourseRepository.findByCurrenciesAndMaxDate(inputTO.getFrom(), inputTO.getTo(), conversionDate)
                .map(currencyPairCourse -> getConvertResult(currencyPairCourse, inputTO))
                .orElse(Optional.empty());
    }

    @Override
    public List<CurrencyFindResultTO> findAllCurrencies(final Optional<LocalDate> inputDate) {
        final LocalDate conversionDate = (inputDate.isPresent()) ? inputDate.get() : LocalDate.now();

        return currencyPairCourseRepository.findAllByCurrenciesAndMaxDate(conversionDate).stream()
                .map(currencyPairCourse -> {
                    final CurrencyFindResultTO resultTO = new CurrencyFindResultTO();
                    resultTO.setFromCurrency(currencyPairCourse.getCurrencyPair().getFromCurrencyCode());
                    resultTO.setToCurrency(currencyPairCourse.getCurrencyPair().getToCurrencyCode());
                    resultTO.setPublishedDate(currencyPairCourse.getPublishedDate());
                    resultTO.setExchangeRate(currencyPairCourse.getExchangeRate());

                    return resultTO;
                })
                .collect(Collectors.toList());
    }

    //TODO @Cacheable
    @Override
    public List<String> findAllCurrencyCodes() {
        return currencyRepository.findAllCurrencyCodes();
    }

    private Optional<ConvertResultTO> getConvertResult(final CurrencyPairCourse currencyPairCourse, final ConvertInputTO inputTO) {
        if(Objects.equals(currencyPairCourse.getCurrencyPair().getFromCurrencyCode(), inputTO.getFrom()) &&
                Objects.equals(currencyPairCourse.getCurrencyPair().getToCurrencyCode(), inputTO.getTo())) {

            final BigDecimal convertedResult =
                    inputTO.getValue().divide(currencyPairCourse.getExchangeRate(), ROUNDING_MODE);
            return Optional.of(new ConvertResultTO(convertedResult));

        } else if(Objects.equals(currencyPairCourse.getCurrencyPair().getFromCurrencyCode(), inputTO.getTo()) &&
                Objects.equals(currencyPairCourse.getCurrencyPair().getToCurrencyCode(), inputTO.getFrom())) {

            final BigDecimal convertedResult =
                    inputTO.getValue().multiply(currencyPairCourse.getExchangeRate());
            return Optional.of(new ConvertResultTO(convertedResult));
        }

        return Optional.empty();
    }

}