package cz.komercpoj.currencyconverter.service;

import cz.komercpoj.currencyconverter.to.CurrencyFindResultTO;
import cz.komercpoj.currencyconverter.to.ConvertInputTO;
import cz.komercpoj.currencyconverter.to.ConvertResultTO;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Service for handling currencies
 */
public interface ICurrenciesService {

    /**
     * converts currencies according to input and returns result
     * @param inputTO input parameters
     * @return converted result
     */
    Optional<ConvertResultTO> convertCurrency(ConvertInputTO inputTO);

    /**
     * finds all supported currencies and return their setup
     * @param conversionDate date of conversion
     * @return all supported currencies with setum
     */
    List<CurrencyFindResultTO> findAllCurrencies(Optional<LocalDate> conversionDate);

    /**
     * finds all supported currenccy codes
     * @return all suported codes
     */
    List<String> findAllCurrencyCodes();
}


