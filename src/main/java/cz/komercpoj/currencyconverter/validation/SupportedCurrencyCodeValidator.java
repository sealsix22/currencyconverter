package cz.komercpoj.currencyconverter.validation;

import cz.komercpoj.currencyconverter.service.ICurrenciesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Service
public class SupportedCurrencyCodeValidator implements ConstraintValidator<SupportedCurrencyCode, String> {

    @Autowired
    private ICurrenciesService iCurrenciesService;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || iCurrenciesService.findAllCurrencyCodes().stream().anyMatch(value::equals);
    }
}
