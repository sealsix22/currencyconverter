package cz.komercpoj.currencyconverter.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = SupportedCurrencyCodeValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface SupportedCurrencyCode {
    String message() default "'${validatedValue}' has to be one of supported currency codes";
    abstract Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
