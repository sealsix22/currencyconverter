package cz.komercpoj.currencyconverter.controller;

import cz.komercpoj.currencyconverter.service.ICurrenciesService;
import cz.komercpoj.currencyconverter.to.CurrencyFindResultTO;
import cz.komercpoj.currencyconverter.to.ConvertInputTO;
import cz.komercpoj.currencyconverter.to.ConvertResultTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Rest controller for Currencies v1
 */
@RestController
@RequestMapping("/api/v1/currencies")
public class CurrenciesRestV1Controller {

    @Autowired
    private ICurrenciesService currenciesService;

    @PostMapping({"", "/{conversionDate}"})
    public ResponseEntity<List<CurrencyFindResultTO>> findAll(
            @PathVariable @DateTimeFormat(iso= DateTimeFormat.ISO.DATE) Optional<LocalDate> conversionDate) {
        return ResponseEntity.ok(currenciesService.findAllCurrencies(conversionDate));
    }

    @PostMapping("/convert")
    public ResponseEntity<ConvertResultTO> convert(@Valid @RequestBody ConvertInputTO convertInputTO) { ;
        return currenciesService.convertCurrency(convertInputTO)
                .map(ResponseEntity::ok)
                .orElseGet(ResponseEntity.notFound()::build);
    }
}

