package cz.komercpoj.currencyconverter.domain.entity;

import cz.komercpoj.currencyconverter.domain.entity.codelist.CurrencyPair;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Holds info currency courses
 */
@Entity(name = "CURRENCY_PAIR_COURSE")
public class CurrencyPairCourse extends UpdatableEntity {

    @OneToOne
    @JoinColumn(name = "CURRENCY_PAIR_ID")
    private CurrencyPair currencyPair;

    @Column(name = "PUBLISHED_DATE")
    private LocalDate publishedDate;

    @Column(name = "EXCHANGE_RATE")
    private BigDecimal exchangeRate;

    public CurrencyPair getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(CurrencyPair currencyPair) {
        this.currencyPair = currencyPair;
    }

    public LocalDate getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(LocalDate publishedDate) {
        this.publishedDate = publishedDate;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }
}
