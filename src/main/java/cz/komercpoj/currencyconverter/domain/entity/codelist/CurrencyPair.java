package cz.komercpoj.currencyconverter.domain.entity.codelist;

import javax.persistence.*;
import java.util.Objects;

/**
 * Holds info about currencies pairs
 */
@Entity(name = "CURRENCY_PAIR")
public class CurrencyPair extends CodelistEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "FROM_CURRENCY_CODE")
    private String fromCurrencyCode;

    @Column(name = "TO_CURRENCY_CODE")
    private String toCurrencyCode;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CurrencyPair that = (CurrencyPair) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getFromCurrencyCode(), that.getFromCurrencyCode()) &&
                Objects.equals(getToCurrencyCode(), that.getToCurrencyCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFromCurrencyCode(), getToCurrencyCode());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFromCurrencyCode() {
        return fromCurrencyCode;
    }

    public void setFromCurrencyCode(String fromCurrencyCode) {
        this.fromCurrencyCode = fromCurrencyCode;
    }

    public String getToCurrencyCode() {
        return toCurrencyCode;
    }

    public void setToCurrencyCode(String toCurrencyCode) {
        this.toCurrencyCode = toCurrencyCode;
    }
}

