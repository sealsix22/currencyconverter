package cz.komercpoj.currencyconverter.to;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

public class ConvertResultTO implements Serializable {

    private static final long serialVersionUID = 2L;

    private BigDecimal result;

    public ConvertResultTO() {
    }

    public ConvertResultTO(BigDecimal result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConvertResultTO that = (ConvertResultTO) o;
        return Objects.equals(getResult(), that.getResult());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getResult());
    }

    public BigDecimal getResult() {
        return result;
    }

    public void setResult(BigDecimal result) {
        this.result = result;
    }
}
