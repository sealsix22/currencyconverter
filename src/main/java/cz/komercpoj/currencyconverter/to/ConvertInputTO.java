package cz.komercpoj.currencyconverter.to;

import cz.komercpoj.currencyconverter.validation.SupportedCurrencyCode;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class ConvertInputTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotBlank
    @SupportedCurrencyCode
    private String from;

    @NotBlank
    @SupportedCurrencyCode
    private String to;

    @Range
    private BigDecimal value;

    //@NotNull
    //@PastOrPresent
    private LocalDate date;

    @AssertTrue(message = "'date' must be present and must be at lease one month old and until now included")
    public boolean isDateValid() {
        final LocalDate now = LocalDate.now();
        return date == null ||
                (date.isAfter(now.minusMonths(1).minusDays(1)) && date.isBefore(now.plusDays(1)));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConvertInputTO that = (ConvertInputTO) o;
        return Objects.equals(getFrom(), that.getFrom()) &&
                Objects.equals(getTo(), that.getTo()) &&
                Objects.equals(getValue(), that.getValue()) &&
                Objects.equals(getDate(), that.getDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFrom(), getTo(), getValue(), getDate());
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
