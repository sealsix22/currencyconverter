package cz.komercpoj.currencyconverter.to;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class CurrencyFindResultTO implements Serializable {

    private static final long serialVersionUID = 5L;

    private String fromCurrency;

    private String toCurrency;

    private LocalDate publishedDate;

    private BigDecimal exchangeRate;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CurrencyFindResultTO that = (CurrencyFindResultTO) o;
        return Objects.equals(getFromCurrency(), that.getFromCurrency()) &&
                Objects.equals(getToCurrency(), that.getToCurrency()) &&
                Objects.equals(getPublishedDate(), that.getPublishedDate()) &&
                Objects.equals(getExchangeRate(), that.getExchangeRate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFromCurrency(), getToCurrency(), getPublishedDate(), getExchangeRate());
    }

    public String getFromCurrency() {
        return fromCurrency;
    }

    public void setFromCurrency(String fromCurrency) {
        this.fromCurrency = fromCurrency;
    }

    public String getToCurrency() {
        return toCurrency;
    }

    public void setToCurrency(String toCurrency) {
        this.toCurrency = toCurrency;
    }

    public LocalDate getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(LocalDate publishedDate) {
        this.publishedDate = publishedDate;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }
}
