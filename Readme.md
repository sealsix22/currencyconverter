#### Currency Converter demo aplikace slouzi pro CRUD (do budoucna) prevod men.

Momentalne je podporovana pouze databaze H2.

* s aktivnim dev profilem se automaticky pusti H2 db i v serverovem modu 
(moznost pripojit pres http://localhost:8081/h2-console/ jmeno a heslo je dbb, viz application-dev.properties) 
a pres liquibase se vytvori tabulky.
* po zbuildeni mavenem (mvn clean package) se pusti testy a potom se da aplikace pustit nasledovne:

```
java -jar  -Dspring.profiles.active=dev  CurrencyConverter-1.0.0.jar
```
*  bez dev profilu by melo jit aplikaci pustit oproti potrebne databazi takto (nezkousel jsem):
```
java -jar CurrencyConverter-1.0.0.jar --spring.datasource.url=jdbc:h2:tcp://localhost:9090/mem:testdb 
--spring.datasource.username=dbb --spring.datasource.password=dbb --server.port=8083
```
#### poznamky:
- entity model nepokryva mozne budouci pozadavky - historizace, soft delete atd.
- neni udelane stahovani devizoveho listku pres asynchronni job
- prevodni kurzy jsou pro kazdy smer pravdepodobne jine, aktualne aplikace pouziva 
pouze jeden kurz pro oba smery. Je ale mozne aplikaci relativne snadno upravit
aby pouzivala ruzme kurzy pro oba smery. Datovy model je na to nachystany.
- chybi job ktery by kazdy den stahoval devizove listky pomoci nejakeho
clienta a externiho api endpointu, pripadne jinym zpusobem
- schwager dokumentace je dostupna na (credentials viz. application.properties):
``` 
 http://localhost:8081/swagger-ui.html
```


#### TODO list:
- pridat nastaveni pocet mist zaokrouhleni, rounding mode u prevodu currencies
- zvazit pouziti JSR 354 
- pravidelne stahovani devizoveho listku pres asynchronni job
- predelat api aby se vic blizilo restful misto rest a RPC hybridu? :)
- soapui test scenare
- generovani TO napr.z yamlu?
- lokalizace hlasek ? u API by ale nemela byt nutna
- v liquibase pridat tablespacy
- zvetsit mnozstvi testu integracnich i jednotkovych
- vyresit jak v integracnich testech pres liquibase insertovat cas posunuty od CURRENT_DATE o pozadovany pocet dni zpet.
 melo by to jit vyresit tak ze budu v liquibase changesetu mit primo sql ktere to generuje. bude to ale specificke pro pouzitou db
- pokud bude hodne dat, zvazit pridani pagination tak aby rest kontrolery strankovaly - napr u nahledu dat muze byt velke mnozstvi vracenych dat
- pridat cachovani dat(CurrenciesService:findAllCurrencyCodes atd.) a evictovani z cache pokud updatuju cachovane data
- pridat releasovani napr pres maven release plugin
- misto rucniho konvertovani TOs pouzit nejaky nereflexivni mapovaci framework jako mapstruct aby to bylo rychle
- zvazit pridani exception resolveru pro lepsi hlasky
- projit pom a pro jistotu zbuildene jar na zbytecnosti a zmensit velikost jar. 46 MB je dost :)
- zjistit jak h2 pouziva indexy a jestli nektere pridane nejsou zbytecne
- liquibase pridat tablespacy
- pridat tool na formatovani a kraceni sql query v logu jako napr log4jdbc (nahrazeni sql projekce za hvezdicku atd)
- pridat logovaci tool jako logback atd






